#!/usr/bin/env python3

import os
import sys
from argparse import ArgumentParser
from configobj import ConfigObj
from datetime import datetime

from sensor_collector import debug
from sensor_collector import exit_constants
from sensor_collector.graceful_interrupt import GracefulInterrupt
from sensor_collector.wrappers import SensorWrapper, StorageWrapper

available_storage_methods = [
    'Local',
    'Console',
    'Dashboard',
    'Spreadsheet',
    'Custom'
]

# Change directory to current directory
os.chdir(os.path.dirname(os.path.abspath(__file__)))


def main():
    try:
        arguments = parse_arguments()
        config = parse_config(arguments.config_files)

        sensor = SensorWrapper(config)

        storage = StorageWrapper(config, available_storage_methods, sensor.sample)

        show_status(sensor.enabled_sensors, storage.enabled_methods)
    except KeyboardInterrupt:
        exit(exit_constants.OK)

    measurements = arguments.number_measurements
    run_forever = measurements == 0

    graceful_interrupt = GracefulInterrupt(exit, exit_constants.OK)

    while not graceful_interrupt.interrupted:
        time_before = datetime.now().timestamp()
        storage.store(sensor.measure())

        if run_forever or measurements > 1:
            if measurements > 1:
                measurements -= 1
            graceful_interrupt.sleep(config['Sensor'].as_int('Interval') - int(datetime.now().timestamp() - time_before))
        else:
            break

    exit(exit_constants.OK)


def parse_arguments():
    parser = ArgumentParser(description='Path to config file(s).')
    parser.add_argument('-f', '--config', dest='config_files', metavar='FILES', type=str, nargs='+',
                        default=['config.ini'], help='configuration used by program, specified from least to most significant')
    parser.add_argument('-n', '--number_measurements', dest='number_measurements', metavar='N', type=int,
                        default=0, help='number of measurements performed before exiting (0 is infinite)')

    return parser.parse_args()


def parse_config(config_files: list = ['config.ini']):
        def read_config(filename):
            if filename == '-':
                return ConfigObj(sys.stdin.readlines(), encoding='UTF8')
            else:
                try:
                    return ConfigObj(filename, file_error=True, encoding='UTF8')
                except IOError:
                    debug.print('error', 'Could not access config file \'%s\'' % filename)
                    exit(exit_constants.CONFIG_NOT_ACCESSIBLE)

        config = read_config(config_files[-1])

        if len(config_files) > 1:
            for config_file in reversed(config_files[:-1]):
                new_config = read_config(config_file)
                new_config.merge(config)
                config = new_config

            debug.print('informational', 'Multiple configuration files has been specified.'
                                         ' Writing to configuration has been disabled.')
            config.filename = None

        return config


def show_status(sensors, storage):
    debug.print('informational', 'Running with sensor(s): %s' % ', '.join(sensors))
    debug.print('informational', 'Running with storage method(s): %s' % ', '.join(storage))


if __name__ == '__main__':
    main()
