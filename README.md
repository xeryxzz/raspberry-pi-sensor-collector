# Raspberry Pi Sensor Collector
## Connecting the SHT20 sensor

Connect your sensor the following way

| Sensor | Raspberry Pi (GPIO) |
|--------|---------------------|
| VCC    | Pin 1               |
| GND    | Pin 6               |
| SDA    | Pin 3 (BCM 2)       |
| SCL    | Pin 5 (BCM 3)       |

For a more visual explaination, have a look at: [https://pinout.xyz/](https://pinout.xyz/)

## Setting up your Raspberry Pi

Flash [Raspbian Stretch Lite](https://www.raspberrypi.org/downloads/raspbian/) to your MicroSD with [Win32Imager](https://sourceforge.net/projects/win32diskimager/)

Navigate to your MicroSD labeled `boot` and place a file named `ssh`

Place the MicroSD into your Raspberry Pi, and power it on

Find the IP address of your Raspberry Pi (Look inside your routers interface)

Connect to the Raspberry Pi using [PuTTY](https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html), username is `pi` and password is `raspberry` by default

## Installation

Upgrade your Raspberry Pi
    
    sudo apt update
    sudo apt upgrade

Install required packages

    sudo apt install git python3 python3-pip python3-smbus python3-rpi.gpio


Configure your Raspberry Pi

    sudo raspi-config
Go through the options `Change User Password`, `Interfacing Options -> I2C` and `Advanced -> Expand Filesystem`

Then reboot

    sudo systemctl reboot

Connect to your Raspberry Pi again and clone the repository

    git clone https://gitlab.com/xeryxzz/raspberry-pi-sensor-collector

Setup the program

    cd raspberry-pi-sensor-collector
    sudo ./setup.py install
    
Configure the program

    cp config.ini.example config.ini
    nano config.ini

Start the program, and check that your configuration works
    
    ./main.py -n 1

## Starting and running the program at boot

Choose either `cron` or `systemd`, not both! 

### Using `cron` (Recommended)

Start the `cron` editor

    crontab -e

Add the following

    */15 * * * * /home/pi/raspberry-pi-sensor-collector/main.py -n 1 > /dev/null
This makes your program run and submit data every 15 minutes

### Using `systemd`
Open up and make sure the unit file correctly configured for your purposes

    nano sensor-collector.service 

Copy the unit file

    sudo cp sensor-collector.service /etc/systemd/system 

Reload unit files

    sudo systemctl daemon-reload

Make your service start at boot

    sudo systemctl enable sensor-collector

Start your service

    sudo systemctl start sensor-collector

## Adding additional sensors

In this example we are connecting an analog LDR sensor in order to measure light/illuminance

Start by connecting your sensor

| Breadboard  | Raspberry Pi (GPIO) |
|-------------|---------------------|
| 3V3         | Pin 17              |
| Data/GPIO   | Pin 22 (BCM 25)     |
| Ground      | Pin 25              |

![LDR Sketch](https://gitlab.com/xeryxzz/raspberry-pi-sensor-collector/raw/master/documentation/images/ldr_sketch.svg)

For this case a 220Ω resistor and a 1µF capacitor was used

Edit your configuration

    nano config.ini
    ...
    [Sensor]
    Models = SHT20, AnalogRC
    Arguments = 1, ["22" "Light" "x" "ms"] 
    ...

The LDR sensor (and any other analog input) uses the `AnalogRC` model, and takes the GPIO pin (`22`) and optionally also the type of measurement (`Light`), the function to apply to the raw sensor data (`x`), and the unit (`ms`) as arguments

Lastly start your program and check that everything is working as expected

    ./main.py -n 1

Note: The `AnalogRC` model by default only outputs the time in milliseconds for the capacitor to charge. To get a proper value and unit you will have to fine-tune and calibrate the function and unit arguments according to your setup. 
