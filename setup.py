#!/usr/bin/env python3

from setuptools import setup

setup(
    name='raspberry-pi-sensor-collector',
    version='1.0',
    install_requires=[
        'google-auth>=1.6.3',
        'google-api-python-client>=1.7.8',
        'google-auth-httplib2>=0.0.3',
        'google-auth-oauthlib>=0.2.0',
        'configobj>=5.0.6',
        'requests>=2.21.0',
        'pyasn1<0.5.0,>=0.4.1'
    ],
)
