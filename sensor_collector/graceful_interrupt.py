import signal
import time


class GracefulInterrupt:
    def __init__(self, exit_function, exit_code=0):
        self.interrupted = False
        self.sleeping = False
        self.__exit_function = exit_function
        self.__exit_code = exit_code
        signal.signal(signal.SIGINT, self.__exit)
        signal.signal(signal.SIGTERM, self.__exit)

    def __exit(self, _signum, _frame):
        self.interrupted = True
        if self.sleeping:
            return self.__exit_function(self.__exit_code)

    def exit(self):
        return self.__exit(None, None)

    def sleep(self, *args, **kwargs):
        if not self.interrupted:
            self.sleeping = True
            time.sleep(*args, **kwargs)
            self.sleeping = False
        else:
            return self.__exit_function(self.__exit_code)
