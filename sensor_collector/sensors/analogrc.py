import time
import RPi.GPIO as GPIO
from math import *

from sensor_collector import measurement_constants
from sensor_collector.sensors.base import BaseSensor, SensorMeasurement


class SensorAnalogRC(BaseSensor):
    def __init__(self, config, index):
        GPIO.setmode(GPIO.BOARD)

        self.__name = measurement_constants.ANALOG
        self.__function = lambda x: x
        self.__unit = measurement_constants.ANALOG_MILLISECONDS

        args = self.__parse_list(config['Sensor']['Arguments'][index])
        if isinstance(args, list):
            self.__pin = int(args[0])
            if len(args) >= 2:
                self.__name = args[1]
                if len(args) >= 3:
                    self.__function = lambda x: eval(args[2])
                    if len(args) >= 4:
                        self.__unit = args[3]
        else:
            self.__pin = int(args)

    @staticmethod
    def __parse_list(config_list):
        if config_list.startswith('[') or config_list.startswith('('):
            stripped = config_list.strip('[()]')
            arguments = []
            argument = ''
            reading = False
            literal = False
            for c in stripped:
                if not literal:
                    if c == '"':
                        reading = not reading
                        if not reading:
                            arguments.append(argument)
                            argument = ''
                    elif c == '\\':
                        literal = True
                    elif reading:
                        argument += c
                elif reading:
                    argument += c
                    literal = False

            return arguments
        else:
            return config_list

    def __read_sensor(self):
        # Discharge capacitor
        GPIO.setup(self.__pin, GPIO.OUT)
        GPIO.output(self.__pin, GPIO.LOW)

        time.sleep(0.1)

        start = time.time()
        GPIO.setup(self.__pin, GPIO.IN)

        # Count loops until voltage across
        # capacitor reads high on GPIO
        while GPIO.input(self.__pin) == GPIO.LOW:
            pass

        return (time.time() - start) * 1000

    @property
    def sample(self):
        return [
            SensorMeasurement(self.__name, 0, self.__unit)
        ]

    def measure(self):
        capacitor_charge_time = self.__read_sensor()
        function_output = self.__function(capacitor_charge_time)

        return [
            SensorMeasurement(self.__name, function_output, self.__unit)
        ]
