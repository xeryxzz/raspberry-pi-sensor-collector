from sensor_collector import measurement_constants
from sensor_collector.sensors.base import BaseSensor, SensorMeasurement

import random


class SensorRandom(BaseSensor):
    @property
    def sample(self):
        return [
            SensorMeasurement(measurement_constants.TEMPERATURE, 0, measurement_constants.TEMPERATURE_CELSIUS),
            SensorMeasurement(measurement_constants.HUMIDITY, 0, measurement_constants.HUMIDITY_RELATIVE_HUMIDITY)
        ]

    def measure(self):
        return [
            SensorMeasurement(measurement_constants.TEMPERATURE, random.uniform(-20, 50), measurement_constants.TEMPERATURE_CELSIUS),
            SensorMeasurement(measurement_constants.HUMIDITY, random.uniform(0, 100), measurement_constants.HUMIDITY_RELATIVE_HUMIDITY)
        ]
