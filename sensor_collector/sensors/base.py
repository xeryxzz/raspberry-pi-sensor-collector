from collections import namedtuple


SensorMeasurement = namedtuple('SensorData', 'type value unit')


class SensorMeasurementCollection(list):
    pass


class BaseSensor:
    def __init__(self, config, index):
        self.__config = config
        self.__index = index

    @property
    def sample(self):
        return []

    def measure(self):
        pass
