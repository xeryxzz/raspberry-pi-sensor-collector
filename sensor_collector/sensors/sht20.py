import smbus
import time

from sensor_collector import measurement_constants
from sensor_collector.sensors.base import BaseSensor, SensorMeasurement


class SensorSHT20(BaseSensor):
    def __init__(self, config, index):
        self.__bus = smbus.SMBus(int(config['Sensor']['Arguments'][index]))

    def __read_sensor(self, command):
        address = 0x40
        self.__bus.write_quick(address)
        self.__bus.write_byte(address, command)
        time.sleep(0.1)
        result = (self.__bus.read_byte(address) << 8)
        result += self.__bus.read_byte(address)
        result &= ~0x0003
        return result

    @property
    def sample(self):
        return [
            SensorMeasurement(measurement_constants.TEMPERATURE, 0, measurement_constants.TEMPERATURE_CELSIUS),
            SensorMeasurement(measurement_constants.HUMIDITY, 0, measurement_constants.HUMIDITY_RELATIVE_HUMIDITY)
        ]

    def measure(self):
        temperature = -46.85 + 175.72 / 65536.0 * float(self.__read_sensor(0xF3))
        humidity = -6.0 + 125.0 / 65536.0 * float(self.__read_sensor(0xF5))

        return [
            SensorMeasurement(measurement_constants.TEMPERATURE, temperature, measurement_constants.TEMPERATURE_CELSIUS),
            SensorMeasurement(measurement_constants.HUMIDITY, humidity, measurement_constants.HUMIDITY_RELATIVE_HUMIDITY)
        ]
