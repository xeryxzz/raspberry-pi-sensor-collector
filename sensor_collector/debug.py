import sys
from typing import Union
from collections import namedtuple

__oldprint = print

TermColor = namedtuple('TermColor', 'name color')
terminal_colors = [
    TermColor('emergency', '\033[0;31m'),
    TermColor('alert', '\033[0;31m'),
    TermColor('critical', '\033[0;31m'),
    TermColor('error', '\033[0;31m'),
    TermColor('warning', '\033[1;33m'),
    TermColor('notice', '\033[0;34m'),
    TermColor('informational', '\033[1;34m'),
    TermColor('debug', '\033[1;37m')
]


def print(level: Union[int, str] = 7, *args, **kwargs) -> None:
    terminal_color = TermColor(None, None)

    if isinstance(level, str):
        terminal_color = next(filter(lambda x: x.name == level, terminal_colors), terminal_colors[7])

    elif isinstance(level, int):
        if 0 <= level <= 7:
            terminal_color = terminal_colors[level]
        else:
            terminal_color = terminal_colors[7]

    sys.stdout.write(terminal_color.color + '[' + terminal_color.name + ']: ')
    __oldprint(*args, **kwargs)
    sys.stdout.write('\033[0m')
