TEMPERATURE = 'Temperature'
TEMPERATURE_CELSIUS = 'C'
TEMPERATURE_FARENHEIT = 'F'
TEMPERATURE_KELVIN = 'K'

HUMIDITY = 'Humidity'
HUMIDITY_RELATIVE_HUMIDITY = '% RH'

ANALOG = 'Analog'
ANALOG_MILLISECONDS = 'ms'
