import json
import requests

from sensor_collector import debug
from sensor_collector import exit_constants
from sensor_collector.storage_methods.base import BaseStorage


class StorageDashboard(BaseStorage):
    def __init__(self, config):
        self.__config = config
        self.__url = None
        self.__token = None
        self.__pairing = None

    def init_storage(self, sensor_sample, interactive):
        if 'Url' not in self.__config['Dashboard'] or not self.__config['Dashboard']['Url']:
            debug.print('critical', '[Dashboard Storage] No address specified by "Url" '
                                    'in configuration. '
                                    'Check your configuration file.')
            exit(exit_constants.DASHBOARD_API_NOT_ACCESSIBLE)
        else:
            self.__url = self.__config['Dashboard']['Url']

        if 'Token' not in self.__config['Dashboard'] or not self.__config['Dashboard']['Token']:
            while not self.__token:
                self.__token = input('[Dashboard Storage] Please paste your sensor key/token: ').strip()

            self.__config['Dashboard']['Token'] = self.__token
            self.__config.write()
        else:
            self.__token = self.__config['Dashboard']['Token']

        measurements = []
        for measurement in sensor_sample:
            dictionary = dict()
            dictionary['name'] = measurement.type
            dictionary['unit'] = measurement.unit
            measurements.append(dictionary)

        out = {'measurements': measurements}
        result = self.__send_post_request('pair', json.dumps(out))

        if result['status'] == 'Paired':
            self.__pairing = result['messages']['measurements']
            debug.print('informational', '[Dashboard Storage] Connected!')

    def store(self, sensor_measurement):
        measurements = []
        for index, measurement in enumerate(sensor_measurement):
            dictionary = dict()
            dictionary['id'] = self.__pairing[index]['id']
            dictionary['value'] = measurement.value
            measurements.append(dictionary)

        out = {'measurements': measurements}
        result = self.__send_post_request('measurement', json.dumps(out))

    def __send_post_request(self, endpoint, data=None):
        headers = {'Content-Type': 'application/json', 'Authorization': 'Bearer %s' % self.__token}
        response = requests.post(self.__url.strip().rstrip('/') + '/api/v1/' + endpoint, headers=headers, data=data)

        if response.status_code == 200:
            return json.loads(response.content.decode('utf-8'))
        else:
            debug.print('critical', '[Dashboard Storage] Error, message from server: %s' % response.content.decode('utf-8'))
            return json.loads(response.content.decode('utf-8'))
