import os.path
import csv
from datetime import datetime

from sensor_collector.storage_methods.base import BaseStorage


class StorageLocal(BaseStorage):
    def __init__(self, config):
        self.__config = config

    def init_storage(self, sensor_sample, interactive):
        if 'File' not in self.__config['Local'] or not os.path.isfile(self.__config['Local']['File']):
            default_filename = 'measurements.csv' if 'File' not in self.__config['Local'] \
                                                  else self.__config['Local']['File']
            filename = default_filename

            while interactive and 'n' in input('[Local Storage] Please confirm filename "%s" (Y/n): ' % filename).lower():
                answer = ''
                while not answer.strip():
                    answer = input('[Local Storage] Please input filename: ')
                filename = answer

            filename = filename.strip() if filename.strip() else default_filename
            self.__config['Local']['File'] = filename
            self.__config.write()

            fields = list(map(lambda x: x.type + ' (' + x.unit + ')', sensor_sample))
            fields.append('Timestamp')
            with open(filename, 'w', newline='') as f:
                writer = csv.writer(f)
                writer.writerow(fields)

    def store(self, measurement):
        row = []
        for field in measurement:
            row.append(field.value)
        row.append(datetime.now())

        with open(self.__config['Local']['File'], 'a', newline='') as f:
            writer = csv.writer(f)
            writer.writerow(row)
