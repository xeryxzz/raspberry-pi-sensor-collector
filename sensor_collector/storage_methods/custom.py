import json
import subprocess
import shlex
import os
from collections import OrderedDict

from sensor_collector import debug
from sensor_collector import exit_constants
from sensor_collector.storage_methods.base import BaseStorage


class StorageCustom(BaseStorage):
    def __init__(self, config):
        self.__config = config
        self.__signature = '' if 'Signature' not in self.__config['Custom'] \
            else self.__config['Custom']['Signature'].strip()
        self.__output_enabled = False if 'Print' not in self.__config['Custom'] \
            else self.__config['Custom'].as_bool('Print')

    def init_storage(self, sensor_sample, interactive):
        if 'ScriptPath' not in self.__config['Custom'] or not self.__config['Custom']['ScriptPath'].strip():
            debug.print('critical', '[Custom Storage] Configuration error. '
                                    'Storage method "Custom" enabled, but "ScriptPath" misconfigured. '
                                    'Check your configuration file.')
            exit(exit_constants.CONFIG_ERROR)
        elif not os.path.isfile(self.__config['Custom']['ScriptPath'].strip()):
            debug.print('critical', '[Custom Storage] Could not find executable specified by "ScriptPath" '
                                    'in configuration. '
                                    'Check your configuration file.')
            exit(exit_constants.CUSTOM_EXECUTABLE_NOT_ACCESSIBLE)
        elif not os.access(self.__config['Custom']['ScriptPath'].strip(), os.X_OK):
            debug.print('critical', '[Custom Storage] File specified by "ScriptPath" is not executable. '
                                    'Check permissions for your custom file.')
            exit(exit_constants.CUSTOM_EXECUTABLE_NOT_ACCESSIBLE)

    def __execute(self, json_string):
        arguments = shlex.split(self.__config['Custom']['ScriptPath'])
        arguments[0] = os.path.abspath(arguments[0])
        process = subprocess.Popen(arguments, stdout=subprocess.PIPE, stdin=subprocess.PIPE, stderr=subprocess.PIPE)
        result = process.communicate(input=str.encode(json_string))

        if result[0] and self.__output_enabled:
            if self.__signature:
                print('%s %s' % (self.__signature, result[0]))
            else:
                print(result[0])
        if result[1]:
            debug.print('warning', '%s %s' % (self.__signature, result[1]))

    def store(self, measurement):
        output = []
        for field in measurement:
            dictionary = OrderedDict()
            dictionary[field.type] = '%f!%s' % (field.value, field.unit)
            output.append(dictionary)
        self.__execute('{' + json.dumps(output) + '}')
