class BaseStorage:
    def __init__(self, config):
        self.__config = config

    def init_storage(self, sensor_sample, interactive):
        pass

    def store(self, measurement):
        pass
