from sensor_collector.storage_methods.base import BaseStorage


class StorageConsole(BaseStorage):
    def store(self, measurement):
        output = ''
        for field in measurement:
            output += '%s: %.2f %s, ' % (field.type, field.value, field.unit)
        print('[Console Storage] %s' % output[:-2])
