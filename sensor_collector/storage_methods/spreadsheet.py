import os.path
import pickle
import string
from datetime import datetime

from sensor_collector import debug
from sensor_collector import exit_constants
from sensor_collector.storage_methods.base import BaseStorage

from googleapiclient.discovery import build
from google_auth_oauthlib.flow import InstalledAppFlow
from google.auth.transport.requests import Request


class StorageSpreadsheet(BaseStorage):
    def __init__(self, config):
        self.__config = config
        self.__sensor_sample = None
        self.__scopes = []
        self.__credentials = None
        self.__sheets_service = None
        self.__drive_service = None
        self.__sheet_upper_range = None

    def init_storage(self, sensor_sample, interactive):
        if 'Sheet' not in self.__config['Spreadsheet'] or not self.__config['Spreadsheet']['Sheet']:
            debug.print('critical', '[Spreadsheet Storage] Configuration error. '
                                    'Storage method "Spreadsheet" enabled, but "Sheet" misconfigured. '
                                    'Check your configuration file.')
            exit(exit_constants.SPREADSHEET_SHEET_MISCONFIGURED)

        self.__sensor_sample = sensor_sample

        self.__scopes = [
            'https://www.googleapis.com/auth/spreadsheets',
            'https://www.googleapis.com/auth/drive'
        ]

        if os.path.exists('token.pickle'):
            with open('token.pickle', 'rb') as token:
                self.__credentials = pickle.load(token)

        if not self.__credentials or not self.__credentials.valid:
            if self.__credentials and self.__credentials.expired and self.__credentials.refresh_token:
                self.__credentials.refresh(Request())

            else:
                credentials_file = 'credentials.json' if 'CredentialsFile' not in self.__config['Spreadsheet'] \
                                                         and self.__config['Spreadsheet']['CredentialsFile'].strip() \
                                                         else self.__config['Spreadsheet']['CredentialsFile'].strip()
                flow = InstalledAppFlow.from_client_secrets_file(credentials_file, self.__scopes)
                self.__credentials = flow.run_console()

            with open('token.pickle', 'wb') as token:
                pickle.dump(self.__credentials, token)

        self.__sheets_service = build('sheets', 'v4', credentials=self.__credentials)
        self.__drive_service = build('drive', 'v3', credentials=self.__credentials)

        self.__sheet_upper_range = string.ascii_uppercase[len(sensor_sample) + 1]

        if 'Id' not in self.__config['Spreadsheet'] or not self.__config['Spreadsheet']['Id']:
            if 'Name' not in self.__config['Spreadsheet'] or not self.__config['Spreadsheet']['Name']:
                self.__config['Spreadsheet']['Name'] = 'Sensor Data (%s)' % datetime.now().strftime("%Y-%m-%d %H:%M")
                self.__config.write()
                self.__create_spreadsheet()
            else:
                response = self.__drive_service.files().list(fields='files(id)',
                                                             q='mimeType="application/vnd.google-apps.spreadsheet" and name = "%s"' % self.__config['Spreadsheet']['Name']).execute()

                if len(response.get('files', [])) == 0:
                    self.__create_spreadsheet()
                else:
                    option_int = 0
                    if len(response.get('files', [])) > 1:
                        print('Multiple files named "%s" detected!' % self.__config['Spreadsheet']['Name'])
                        for index, file in enumerate(response.get('files', []), start=1):
                            print('%d. %s' % (index, file.get('id')))
                        while True:
                            option = input('Select a file ID: ')
                            try:
                                option_int = int(option)
                            except ValueError:
                                print('Please select a valid option.')

                            if 0 < option_int <= len(response.get('files', [])):
                                option_int -= 1
                                break
                            else:
                                print('Please select a valid option.')
                    self.__config['Spreadsheet']['Id'] = response.get('files', [])[option_int].get('id')
                    self.__config.write()

        self.__create_sheet_if_not_exist()

    def store(self, measurement):
        values = [field.value for field in measurement]
        values.extend(datetime.now().strftime("%Y-%m-%d %H.%M.%S").split(' '))
        self.__append_row(values)

    def __create_spreadsheet(self):
        spreadsheet = self.__sheets_service.spreadsheets().create(body={'properties':
                                                                            {'title': self.__config['Spreadsheet']['Name']}
                                                                        }, fields='spreadsheetId').execute()
        self.__config['Spreadsheet']['Id'] = spreadsheet.get('spreadsheetId')

        self.__create_sheet_if_not_exist()

        debug.print('informational', '[Spreadsheet Storage] Created spreadsheet named "%s", data will be displayed on sheet "%s"' % (self.__config['Spreadsheet']['Name'], self.__config['Spreadsheet']['Sheet']))
        self.__config.write()

    def __create_sheet_if_not_exist(self):
        response = self.__sheets_service.spreadsheets().get(spreadsheetId=self.__config['Spreadsheet']['Id']).execute()
        exists = any((True for sheet in response.get('sheets') if sheet['properties']['title'] == self.__config['Spreadsheet']['Sheet']))

        if not exists:
            self.__sheets_service.spreadsheets().batchUpdate(spreadsheetId=self.__config['Spreadsheet']['Id'],
                                                         body={
                                                             'requests': {
                                                                 'addSheet': {
                                                                     'properties': {
                                                                         'title': self.__config['Spreadsheet']['Sheet']
                                                                     }
                                                                 }
                                                             }
                                                         }).execute()

            values = ["%s (%s)" % (field.type, field.unit) for field in self.__sensor_sample]
            values.extend(('Date', 'Time'))
            self.__append_row(values)

        return exists

    def __append_row(self, values):
        response = self.__sheets_service.spreadsheets().values().append(spreadsheetId=self.__config['Spreadsheet']['Id'],
                                                                        range='%s!A:%s' % (self.__config['Spreadsheet']['Sheet'], self.__sheet_upper_range),
                                                                        valueInputOption='USER_ENTERED',
                                                                        insertDataOption='INSERT_ROWS',
                                                                        body={
                                                                 'range': '%s!A:%s' % (self.__config['Spreadsheet']['Sheet'], self.__sheet_upper_range),
                                                                 'majorDimension': 'ROWS',
                                                                 'values': [values]
                                                             }).execute()
        return response
