from sensor_collector import measurement_constants
from sensor_collector.sensors.base import SensorMeasurement, SensorMeasurementCollection

import importlib
import sys
from itertools import chain


class StorageWrapper:
    def __init__(self, config, available_storage_methods, sensor_sample):
        self.__config = config
        self.__available_storage_methods = available_storage_methods
        self.__sensor_sample = sensor_sample
        self.__enabled_storage_methods = self.__get_enabled_storage_methods()
        self.__storage_instances = self.__instantiate()

    def __get_enabled_storage_methods(self):
        methods = []
        for method in self.__available_storage_methods:
            if self.__config[method].as_bool('Enabled'):
                methods.append(method)
        return methods

    def __instantiate(self):
        instances = []
        for method in self.__enabled_storage_methods:
            module_name = 'sensor_collector.storage_methods.' + method.lower()
            module = importlib.import_module(module_name)
            storage_class = getattr(module, 'Storage' + method)
            instance = storage_class(self.__config)
            instance.init_storage(self.__sensor_sample, sys.stdin.isatty())
            instances.append(instance)
        return instances

    @property
    def enabled_methods(self):
        return self.__enabled_storage_methods

    def store(self, measurement):
        for instance in self.__storage_instances:
            instance.store(measurement)


class SensorWrapper:
    def __init__(self, config):
        self.__config = config
        self.__models = config['Sensor']['Models']
        self.__instances = self.__instantiate()

    def __instantiate(self):
        instances = []
        for index, sensor in enumerate(self.__models):
            module_name = 'sensor_collector.sensors.' + sensor.lower()
            module = importlib.import_module(module_name)
            sensor_class = getattr(module, 'Sensor' + sensor)
            instance = sensor_class(self.__config, index)
            instances.append(instance)
        return instances

    @property
    def enabled_sensors(self):
        return self.__models

    @property
    def sample(self):
        return self.__filter_all(SensorMeasurementCollection(
            chain.from_iterable(instance.sample for instance in self.__instances)
        ))

    def measure(self):
        return self.__filter_all(SensorMeasurementCollection(
            chain.from_iterable(instance.measure() for instance in self.__instances)
        ))

    def __filter_all(self, measurement):
        measurement = self.__temperature_filter(measurement)
        return measurement

    def __temperature_filter(self, measurement):
        if 'TemperatureUnit' in self.__config['Sensor']:
            temperature_field_index = [index for index, field in enumerate(measurement) if field.type.lower() == measurement_constants.TEMPERATURE.lower()][0]
            config_unit = self.__config['Sensor']['TemperatureUnit'].lower()

            unit = measurement[temperature_field_index].unit
            value = measurement[temperature_field_index].value

            if config_unit == 'farenheit' and unit.lower() == measurement_constants.TEMPERATURE_CELSIUS.lower():
                unit = measurement_constants.TEMPERATURE_FARENHEIT
                value = 32 + (9 * value / 5)

            elif config_unit == 'kelvin' and unit.lower() == measurement_constants.TEMPERATURE_CELSIUS.lower():
                unit = measurement_constants.TEMPERATURE_KELVIN
                value = value + 273.15

            measurement[temperature_field_index] = SensorMeasurement(measurement_constants.TEMPERATURE, value, unit)

        return measurement
